package com.itau.custodiaCliente.DTO;

public class OperacaoClienteDTO {
    private String cpf;
    private String senha;
    private char permitirConsultaExterna;

    public OperacaoClienteDTO(String cpf, String senha, char permitirConsultaExterna) {
        this.cpf = cpf;
        this.senha = senha;
        this.permitirConsultaExterna = permitirConsultaExterna;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public char getPermitirConsultaExterna() {
        return permitirConsultaExterna;
    }

    public void setPermitirConsultaExterna(char permitirConsultaExterna) {
        this.permitirConsultaExterna = permitirConsultaExterna;
    }


}
