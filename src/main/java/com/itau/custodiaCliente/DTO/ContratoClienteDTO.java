package com.itau.custodiaCliente.DTO;

import java.util.Date;

public class ContratoClienteDTO {
    private Long numero_contrato;
    private Date dt_inicio;
    private double valor_pago;
    private char consultaExterna;
    private String status_contrato;
    private double valor_total_contrato;
    private double valor_aberto_contrato;
    private Date dt_fim_contrato;

    public ContratoClienteDTO(){}


    public ContratoClienteDTO(Long numero_contrato, Date dt_inicio, double valor_pago, char consultaExterna, String status_contrato, double valor_total_contrato, double valor_aberto_contrato, Date dt_fim_contrato) {
        this.numero_contrato = numero_contrato;
        this.dt_inicio = dt_inicio;
        this.valor_pago = valor_pago;
        this.consultaExterna = consultaExterna;
        this.status_contrato = status_contrato;
        this.valor_total_contrato = valor_total_contrato;
        this.valor_aberto_contrato = valor_aberto_contrato;
        this.dt_fim_contrato = dt_fim_contrato;
    }

    public ContratoClienteDTO(char consultaExterna) {
        this.consultaExterna = consultaExterna;
    }

    public Long getNumero_contrato() {
        return numero_contrato;
    }

    public void setNumero_contrato(Long numero_contrato) {
        this.numero_contrato = numero_contrato;
    }

    public Date getDt_inicio() {
        return dt_inicio;
    }

    public void setDt_inicio(Date dt_inicio) {
        this.dt_inicio = dt_inicio;
    }

    public double getValor_pago() {
        return valor_pago;
    }

    public void setValor_pago(double valor_pago) {
        this.valor_pago = valor_pago;
    }

    public char getConsultaExterna() {
        return consultaExterna;
    }

    public void setConsultaExterna(char consultaExterna) {
        this.consultaExterna = consultaExterna;
    }

    public String getStatus_contrato() {
        return status_contrato;
    }

    public void setStatus_contrato(String status_contrato) {
        this.status_contrato = status_contrato;
    }

    public double getValor_total_contrato() {
        return valor_total_contrato;
    }

    public void setValor_total_contrato(double valor_total_contrato) {
        this.valor_total_contrato = valor_total_contrato;
    }

    public double getValor_aberto_contrato() {
        return valor_aberto_contrato;
    }

    public void setValor_aberto_contrato(double valor_aberto_contrato) {
        this.valor_aberto_contrato = valor_aberto_contrato;
    }

    public Date getDt_fim_contrato() {
        return dt_fim_contrato;
    }

    public void setDt_fim_contrato(Date dt_fim_contrato) {
        this.dt_fim_contrato = dt_fim_contrato;
    }
}
