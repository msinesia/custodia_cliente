package com.itau.custodiaCliente.repository;

import com.itau.custodiaCliente.model.ContratoPF;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustodiaClienteRepository extends CrudRepository <ContratoPF, Long> {
    ContratoPF findByCpf(String cpf);



}
