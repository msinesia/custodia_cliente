package com.itau.custodiaCliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@SpringBootApplication
public class CustodiaClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustodiaClienteApplication.class, args);
	}

}
