package com.itau.custodiaCliente.controller;

import com.itau.custodiaCliente.DTO.ContratoClienteDTO;
import com.itau.custodiaCliente.service.CustodiaClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private CustodiaClienteService custodiaClienteService;

    ContratoClienteDTO contrato = new ContratoClienteDTO();

    @RequestMapping(path="/{cpf},{senha},{consultaExterna}",method = RequestMethod.PUT)
    public void atualizarAutorizacao(@PathVariable String cpf, @PathVariable String senha, @PathVariable char consultaExterna){

        if (custodiaClienteService.validaCliente(cpf,senha)){

            custodiaClienteService.cfgConsultaExterna(consultaExterna);
        }
    }

    @GetMapping
    public ContratoClienteDTO consultarContrato(@RequestParam String cpf, @RequestParam String senha){

        if (custodiaClienteService.validaCliente(cpf,senha)){
            contrato = custodiaClienteService.getContratoCliente(cpf);

        }
        return contrato;

    }
}



