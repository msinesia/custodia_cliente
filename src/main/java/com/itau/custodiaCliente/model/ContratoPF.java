package com.itau.custodiaCliente.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class ContratoPF {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_contrato;
    private String cpf;
    private Date dt_inicio_contrato;
    private Date dt_fim_contrato;
    private String status_contrato;
    private double valor_total_contrato;
    private double valor_aberto_contrato;
    private double valor_pago_contrato;
    private char consulta_externa_cfg;

    public ContratoPF(Date dt_fim_contrato, String status_contrato, double valor_total_contrato, double valor_aberto_contrato) {
        this.dt_fim_contrato = dt_fim_contrato;
        this.status_contrato = status_contrato;
        this.valor_total_contrato = valor_total_contrato;
        this.valor_aberto_contrato = valor_aberto_contrato;
    }

    public ContratoPF(Long id_contrato, String cpf, Date dt_inicio_contrato, Date dt_fim_contrato, String status_contrato, double valor_total_contrato, double valor_aberto_contrato, double valor_pago_contrato, char consulta_externa_cfg) {
        this.id_contrato = id_contrato;
        this.cpf = cpf;
        this.dt_inicio_contrato = dt_inicio_contrato;
        this.dt_fim_contrato = dt_fim_contrato;
        this.status_contrato = status_contrato;
        this.valor_total_contrato = valor_total_contrato;
        this.valor_aberto_contrato = valor_aberto_contrato;
        this.valor_pago_contrato = valor_pago_contrato;
        this.consulta_externa_cfg = consulta_externa_cfg;
    }

    public Long getId_contrato() {
        return id_contrato;
    }

    public void setId_contrato(Long id_contrato) {
        this.id_contrato = id_contrato;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDt_inicio_contrato() {
        return dt_inicio_contrato;
    }

    public void setDt_inicio_contrato(Date dt_inicio_contrato) {
        this.dt_inicio_contrato = dt_inicio_contrato;
    }

    public Date getDt_fim_contrato() {
        return dt_fim_contrato;
    }

    public void setDt_fim_contrato(Date dt_fim_contrato) {
        this.dt_fim_contrato = dt_fim_contrato;
    }

    public String getStatus_contrato() {
        return status_contrato;
    }

    public void setStatus_contrato(String status_contrato) {
        this.status_contrato = status_contrato;
    }

    public double getValor_total_contrato() {
        return valor_total_contrato;
    }

    public void setValor_total_contrato(double valor_total_contrato) {
        this.valor_total_contrato = valor_total_contrato;
    }

    public double getValor_aberto_contrato() {
        return valor_aberto_contrato;
    }

    public void setValor_aberto_contrato(double valor_aberto_contrato) {
        this.valor_aberto_contrato = valor_aberto_contrato;
    }

    public double getValor_pago_contrato() {
        return valor_pago_contrato;
    }

    public void setValor_pago_contrato(double valor_pago_contrato) {
        this.valor_pago_contrato = valor_pago_contrato;
    }

    public char getConsulta_externa_cfg() {
        return consulta_externa_cfg;
    }

    public void setConsulta_externa_cfg(char consulta_externa_cfg) {
        this.consulta_externa_cfg = consulta_externa_cfg;
    }




}

